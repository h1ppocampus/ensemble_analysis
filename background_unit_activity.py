import numpy as np
from scipy import signal as scipysignal
import matplotlib.pyplot as plt


def crossing_threshold(v, threshold, greater_than=True):
    if greater_than:
        crossing_index_tuple = np.where(np.diff((v >= threshold).astype(np.byte), axis=0) > 0)
    else:
        crossing_index_tuple = np.where(np.diff((v <= threshold).astype(np.byte), axis=0) > 0)
    crossing_index = [crossing_index_tuple[0] + 1, crossing_index_tuple[1]]
    return crossing_index


class BackgroundUnitActivity:
    def __init__(self, signal, spike_cutoff=4, t_remove_forward=0.003, t_remove_backward=0.001, sample_rate=30000,
                 highpass=True, highpass_thresh=300, filter_order=3, plot_check=False, plot_channel_n=None):

        self.signal = signal
        self.spike_cutoff = spike_cutoff
        self.t_remove_backward = t_remove_backward
        self.index_remove_backward = np.ceil(t_remove_backward * sample_rate)
        self.t_remove_forward = t_remove_forward
        self.index_remove_duration = np.ceil(t_remove_forward * sample_rate) + self.index_remove_backward
        self.sample_rate = sample_rate
        self.highpass = highpass
        self.highpass_thresh = highpass_thresh
        self.nyquist_highpass_thresh = highpass_thresh / (sample_rate/2)
        self.filter_order = filter_order
        self.spike_epoch_removed = None
        self.plot_check = plot_check
        self.plot_channel_n = plot_channel_n

    def highpass_filtfilt(self):
        b, a = scipysignal.butter(self.filter_order, self.nyquist_highpass_thresh, btype='high')
        self.signal = scipysignal.filtfilt(b, a, self.signal, axis=0)

    def remove_spikes(self):
        if self.plot_check:
            plt.plot(self.signal[:, self.plot_channel_n])
            plt.show()
            cut_off = self.spike_cutoff * np.std(self.signal[:, self.plot_channel_n])
            sig_mean = np.mean(self.signal[:, self.plot_channel_n])

        test = self.signal.copy()

        spike_fancy_index_pos = crossing_threshold(self.signal, np.mean(self.signal, axis=0)
                                                   + self.spike_cutoff * np.std(self.signal, axis=0))

        spike_fancy_index_neg = crossing_threshold(self.signal, np.mean(self.signal, axis=0)
                                                   - self.spike_cutoff * np.std(self.signal, axis=0),
                                                   greater_than=False)

        spike_fancy_index = [np.concatenate([spike_fancy_index_pos[0], spike_fancy_index_neg[0]]),
                             np.concatenate([spike_fancy_index_pos[1], spike_fancy_index_neg[1]])]

        print(spike_fancy_index[0].shape)

        del spike_fancy_index_pos, spike_fancy_index_neg

        spike_fancy_index[0] = spike_fancy_index[0] - self.index_remove_backward

        self.spike_epoch_removed = []
        self.spike_epoch_removed.append(np.concatenate([spike_fancy_index[0].reshape(-1, 1),
                                                        (spike_fancy_index[0] +
                                                         self.index_remove_duration).reshape(-1, 1)], axis=1))
        self.spike_epoch_removed.append(spike_fancy_index[1])

        rep_fancy_index = [np.repeat(spike_fancy_index[0].reshape(-1, 1), self.index_remove_duration, axis=1),
                           np.repeat(spike_fancy_index[1].reshape(-1, 1), self.index_remove_duration, axis=1)
                           .reshape(-1)]
        rep_fancy_index[0] = (rep_fancy_index[0] + range(int(self.index_remove_duration))).reshape(-1)

        remove_epoch = (rep_fancy_index[0] < 0) | \
                       (rep_fancy_index[0] >= self.signal.shape[0])
        rep_fancy_index[0] = np.delete(rep_fancy_index[0], remove_epoch)
        rep_fancy_index[1] = np.delete(rep_fancy_index[1], remove_epoch)
        print(spike_fancy_index[0].shape)

        del remove_epoch


        spike_logic = np.zeros(self.signal.shape, dtype='bool')
        spike_logic[rep_fancy_index[0].astype('int'), rep_fancy_index[1].astype('int')] = 1

        spike_index_total = np.count_nonzero(spike_logic, axis=0)
        print(np.sum(spike_index_total))
        n_reps = np.ceil(spike_index_total / (self.signal.shape[0] - spike_index_total))
        print(n_reps)

        for i, signal_channel in enumerate(self.signal.T):
            # check transpose
            spike_free_sig = np.repeat(signal_channel[~spike_logic[:, i]], n_reps[i])
            print(np.count_nonzero(~spike_logic[:, i], axis=0))
            print(spike_free_sig.shape)

            self.signal[spike_logic[:, i], i] = spike_free_sig[range(spike_index_total[i])]

        if self.plot_check:
            plt.plot(self.signal[:, self.plot_channel_n])

            plt.plot(np.array([0, self.signal.shape[0]]), np.array([sig_mean + cut_off, sig_mean + cut_off]), color='black')
            plt.plot(np.array([0, self.signal.shape[0]]), np.array([sig_mean - cut_off, sig_mean - cut_off]), color='black')



    def lowpass_rectified(self):

        b, a = scipysignal.butter(self.filter_order, self.nyquist_highpass_thresh, btype='low')
        self.signal = scipysignal.filtfilt(b, a, np.abs(self.signal), axis=0)

    def generate_BUA(self):

        if self.highpass:
            self.highpass_filtfilt()
        self.remove_spikes()
        self.lowpass_rectified()


class MUA:
    def __init__(self, signal, spike_cutoff=4, sample_rate=30000, highpass=False, highpass_cutoff=300, filter_order=3):

        self.signal = signal
        self.spike_cutoff = spike_cutoff
        self.sample_rate = sample_rate
        self.spike_fancy_index = None
        self.spike_list = None
        self.highpass = highpass
        self.highpass_cutoff = highpass_cutoff
        self.nyquist_highpass_thresh = highpass_cutoff / (sample_rate/2)
        self.filter_order = filter_order

    def highpass_filtfilt(self):
        b, a = scipysignal.butter(self.filter_order, self.nyquist_highpass_thresh, btype='high')
        self.signal = scipysignal.filtfilt(b, a, self.signal, axis=0)

    def mua_spike_index(self):

        spike_fancy_index_pos = crossing_threshold(self.signal, np.mean(self.signal, axis=0)
                                                   + self.spike_cutoff * np.std(self.signal, axis=0))

        spike_fancy_index_neg = crossing_threshold(self.signal, np.mean(self.signal, axis=0)
                                                   - self.spike_cutoff * np.std(self.signal, axis=0),
                                                   greater_than=False)

        self.spike_fancy_index = [np.concatenate([spike_fancy_index_pos[0], spike_fancy_index_neg[0]]),
                                  np.concatenate([spike_fancy_index_pos[1], spike_fancy_index_neg[1]])]

        self.spike_list = []
        for channel_n in np.unique(self.spike_fancy_index[1]):
            self.spike_list.append(self.spike_fancy_index[0][self.spike_fancy_index[1] == channel_n])

    def exract_mua_spikes(self):

        if self.highpass:
            self.highpass_filtfilt()
        self.mua_spike_index()


