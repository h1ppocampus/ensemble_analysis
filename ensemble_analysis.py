import numpy as np
import pandas as pd
from sklearn import decomposition
import random
import scipy
import matplotlib.pyplot as plt
import torch


def benjamini_hochberg(p_values, fdr=0.05):

    '''

    :param p_values: p-values in a 1d numpy array
    :param fdr: the false discovery rate
    :return: an array of bools where sig_p_values[i] is 1 where p_values[i] is significant and 0 otherwise
    '''

    assert p_values.ndim == 1, 'p-values must be in a 1d array'
    assert p_values.size > 0, 'p-values.size must be greater than 0'
    assert np.count_nonzero((0 <= p_values) & (p_values <= 1)) == p_values.size, \
        'p-values must be between 0 and 1, inclusive'
    assert (0 <= fdr) & (fdr <= 1), 'fdr must be between 0 and 1'

    p_sorted = np.sort(p_values)
    m = p_sorted.shape[0]

    cutoff = fdr*np.array(range(1, m+1))/m
    index = np.where(p_sorted < cutoff)[0]

    if index.size == 0:
        sig_p_values = np.zeros(p_values.shape[0]).astype('bool_')
    else:
        sig_p_values = p_values <= p_sorted[index[-1]]

    return sig_p_values


def bin_spike_times(spike_list, n_samples, sample_rate=20000, bin_duration=0.025):

    '''

    :param spike_list: A list of 1d arrays containing the sample index at which a spike occurs for a single unit
    :param n_samples: The total number of ephys samples obtained over the epoch which spike_list is from
    :param sample_rate: The sampling rate
    :param bin_duration: The desired duration of bins
    :return: A n_bins x n_neurons matrix containing binned spike counts
    '''

    bin_index_dur = np.floor(bin_duration * sample_rate)
    n_bins = np.floor(n_samples/bin_index_dur)
    index_end = np.floor(bin_index_dur * n_bins)

    n_neurons = len(spike_list)

    binned_spike_rate = np.zeros((int(n_bins), n_neurons))
    for i, neurons in enumerate(spike_list):
        hist, _ = np.histogram(neurons, bins=int(n_bins),
                               range=(0, index_end), density=False)
        binned_spike_rate[:, i] = hist

    return binned_spike_rate


def jitter_in_range(neuron, jitter_t_range, n_samples, sample_rate=20000):

    '''
    Jitters spike index by a random number selected from a uniform distrubution in the range -jitter_t_range to
    jitter_t_range
    :param neuron: The spike indices of a single-unit in a 1d np array
    :param jitter_t_range: The range of times (s) over which jitter will be selected.
    :param n_samples: The total number of samples which spike indices are from
    :param sample_rate: The sampling rate (Hz)
    :return: The jittered spike indices of neuron, within the range 0 to n_samples - 1
    '''

    jittered_neuron = neuron + (np.random.rand(neuron.shape[0]) - 1 / 2) * 2 * sample_rate * jitter_t_range
    outside_range = np.logical_or(jittered_neuron < 0, jittered_neuron >= n_samples)
    if np.count_nonzero(outside_range) > 0:
        jittered_neuron[outside_range] = jitter_in_range(neuron[outside_range], n_samples, jitter_t_range, sample_rate)
    return jittered_neuron


def zscore(matrix, axis=0):

    '''

    :param matrix: A 1d or 2d array
    :param axis: The axis along which the z-score is computed
    :return: Z-scored matrix
    '''

    return (matrix - matrix.mean(axis=axis))/np.std(matrix, axis=axis)


def gaussian(x, mean, std):

    '''

    :param x: An nd array containing the x values which the Gaussian function is applied to
    :param mean: The mean of the Gaussian
    :param std: The std of the Gaussian
    :return: An array (Y) where Y[i, j] = f(x[i, j]) and f(x) is the Gaussian function
    '''

    return np.exp((((x-mean)/std)**2)/-2)/((2*np.pi)**(1/2)*std)


def outer_product_1d_vector(V):

    '''

    :param V:  A 1d array
    :return: The outer product of V
    '''

    V_2d = V.reshape((-1, 1))
    outer_product = V_2d @ V_2d.T

    return outer_product


def outer_product_main_diag_0(V):

    '''

    :param V: A 1d array
    :return: The outer product of V with the main diag set to 0
    '''

    outer_product = outer_product_1d_vector(V)
    outer_product = outer_product - np.diag(np.diag(outer_product))

    return outer_product


def outer_product_2d(V_2d, axis=0, zero_main_diag=True):

    '''

    :param V_2d: A 2d array
    :param axis: The axis along which 1d arrays are selected for calculating the outer product
    :param zero_main_diag: If true, the main diag of the outer product is set to 0
    :return: A 3d array (Y) where Y[:, :, i] is the outer product of V_2d[:, i] for axis=0 or V_2d[i,:]
    for axis=1
    '''

    assert axis == 0 or axis == 1, 'Axis must be 0 or 1'
    assert type(zero_main_diag) == type(True), 'zero_main_diag must be a bool'

    if axis == 0:
        V_2d = V_2d.T

    outer_product = np.zeros((V_2d.shape[1], V_2d.shape[1], V_2d.shape[0]))
    for i, V in enumerate(V_2d):

        if zero_main_diag == True:
            outer_product[:, :, i] = outer_product_main_diag_0(V)
        else:
            outer_product[:, :, i] = outer_product_1d_vector(V)

    return outer_product


def spikes_in_epoch(spiking_list, epoch):

    '''

    :param spiking_list:  A list of 1d arrays containing the sample index at which a spike occurs for a single unit
    :param epoch: A 2 element array where epoch[0] represents the start sample-index of the epoch and
    epoch[1] represents the end sample-index of the epoch
    :return: A list of 1d arrays containing the sample index at which a spike occurs for a single unit within the
    specified epochs
    '''

    spikes_in_range = []
    samples_in_epoch = epoch[1] - epoch[0]
    for i, spike_train in enumerate(spiking_list):
        use_spikes = np.logical_and(0 < (spike_train - epoch[0]), (spike_train - epoch[0]) < samples_in_epoch)
        spikes_in_range.append(spike_train[use_spikes]-epoch[0])
        # spike_in_range_not_zeroed = spikes_in_range.append(spike_train[use_spikes]-epoch[0])

    return spikes_in_range, samples_in_epoch


def pca_torch(z_scored_signal):

    '''
    Carries out PCA in a GPU friendly way
    :param z_scored_signal: Columns presents a z-scored time series
    :return:
    eig_value: a 1d tensor containing eigenvalues in ascending order
    eig_vector: a 2d matrix where columns represent eigenvectors. eig_valye[i] is the eigenvalue for eig_vector[:, i]
    '''

    corr_mat = torch.mm(z_scored_signal.t(), z_scored_signal/(z_scored_signal.size()[0] - 1))
    eig_values, eig_vectors = torch.linalg.eigh(corr_mat)
    return eig_values, eig_vectors


def not_too_corr(spike_list, max_index, sample_rate=20000, bin_duration=0.0015, R_cutoff=0.3, spikes_per_neuron=None):

    binned_spikes = bin_spike_times(spike_list, max_index, sample_rate=sample_rate, bin_duration=bin_duration)
    n_neurons = binned_spikes.shape[1]
    corr_mat = np.corrcoef(binned_spikes.T)
    corr_mat_l = corr_mat - np.triu(corr_mat)
    neurons = np.where(corr_mat_l > R_cutoff)
    if spikes_per_neuron is None:
        spikes_per_neuron = np.sum(binned_spikes, axis=0)
    more_spikes_in_1_than_2 = spikes_per_neuron[neurons[0]] > spikes_per_neuron[neurons[1]]
    remove_these_neurons = np.concatenate([neurons[0][more_spikes_in_1_than_2], neurons[1][~more_spikes_in_1_than_2]])
    remove_these_neurons_logic = np.full(n_neurons, False)
    remove_these_neurons_logic[remove_these_neurons] = True
    return remove_these_neurons_logic


class AssemblyPattern:

    def __init__(self, pooling_mode='bin', signal=None, spiking_list=None, n_samples=None, use_spikes_in_range=None,
                 sample_rate=20000, bin_duration=0.025, gpu_circshift=False, device=None,
                 structure_list=None, minimum_spikes=100, sig_mode='mp', cs_perc_cutoff=0.05, ica_iterations=200,
                 mc_correction='bf', circular_its=1000, member_cut_off_mode='std', cut_off=2, zscore_spike_count=True,
                 remove_high_corr=False, remove_high_corr_bin=0.0015, remove_r_cutoff=0.3):

        '''

        :param spiking_list: A list of 1d arrays containing the sample index at which a spike occurs for a single unit
        :param use_spikes_in_range: A 2d array of shape n_epochs x 2 where use_spikes_in_range[n, 0] represents the start
         sample-index of epoch n and use_spikes_in_range[n, 1] represents the end sample-index of epoch n
        :param n_samples: The total number of ephys samples
        :param sample_rate: Sampling rate
        :param bin_duration: The duration of bins (s)
        :param structure_list: List where element stucture_list[i] is the structure that the SU that's spikes are in
        spiking_list[i] are from
        :param minimum_spikes: The minimum number of spikes for a neuron to be included in analysis
        :param sig_mode: If 'cs', a circular shift method will be used to identify significant ensembles. If 'mp', the
        Marchenko-Pastur law is used.
        :param cs_perc_cutoff: The percentage cut-off for significance. For alpha= 0.05, use 5.
        :param mc_correction: If None, no multiple comparison correction is used for the circular shift method. If 'bf',
        the Bonferroni correction is used.
        :param circular_its: The number of iterations for the circular shift method.
        :param member_cut_off_mode: If 'std', the standard deviation will be used to identify members. If perc, the
        percentile will be used.
        :param cut_off: If member_cut_off_mode is 'std', a neuron with weight > cut_off*std will be considered a member.
        If member_cut_off_mode is 'perc', the
        :param zscore_spike_count: If True, the binned spike count will be z-scored in the extract_ensembles method.
        '''

        # add assert that spike_list is a list and that it contains 1d arrays
        # add assert that n_samples cannot be None

        assert sig_mode == 'mp' or sig_mode == 'cs', 'Not a supported sig_mode'
        assert member_cut_off_mode == 'std' or member_cut_off_mode == 'perc', 'Not a supported member_cutoff_mode'
        if cut_off == 'std':
            assert 0 < cut_off, 'cut_off outside range'
        if cut_off == 'perc':
            assert 0 < cut_off < 100, 'cut_off outside range'
        assert type(zscore_spike_count) == type(True), 'zscored_spike_counts must be a bool'
        assert mc_correction == 'bf' or mc_correction is None, 'Not a supported mc_correction'
        if pooling_mode == 'bin':
            assert spiking_list is not None, 'If need_binning=False, spike_list cannot be None'
        elif pooling_mode == 'dif':
            assert signal is not None, 'If need_binning=False, signal cannot be None'
        elif pooling_mode is None:
            assert signal is not None, 'If need_binning=False, signal cannot be None'
        else:
            raise ValueError('pooling_mode not supported')
        assert type(gpu_circshift) == type(True), 'gpu_circshift must be a bool'
        if gpu_circshift:
            assert device is not None, 'For gpu_circshift=True, device cannot be none'
        if remove_high_corr:
            assert n_samples is not None, 'If remove_high_corr=True, n_samples cannot be none'

        self.pooling_mode = pooling_mode
        if pooling_mode == 'dif':
            self.signal = signal
            self.n_neurons = signal.shape[1]
        if pooling_mode is None:
            self.processed_signal = signal
            self.n_neurons = signal.shape[1]
        if pooling_mode == 'bin':
            self.processed_signal = None
            self.processed_signal_prezscore = None
            self.n_neurons = len(spiking_list)
        self.spiking_list = spiking_list
        self.jittered_spiking_list = None
        self.use_spikes_in_range = use_spikes_in_range
        self.binned_list_in_range = None
        self.zscore_spike_count = zscore_spike_count
        self.n_samples = n_samples
        self.sample_rate = sample_rate  # Hz
        self.bin_duration = bin_duration  # ms
        self.minimum_spikes = minimum_spikes

        self.sig_mode = sig_mode
        self.circular_its = circular_its
        self.cs_perc_cutoff = cs_perc_cutoff
        self.mc_correction = mc_correction
        self.gpu_circshift = gpu_circshift
        self.device = device
        self.cs_eigenvalue_dist = None
        self.sig_thresh = None
        self.sig_eigenvectors = None

        if pooling_mode == 'bin':
            self.unit_id = np.array(range(len(spiking_list)))
        else:
            self.unit_id = np.array(range(signal.shape[1]))

        if structure_list is not None:
            self.structure_list = np.array(structure_list)
        else:
            self.structure_list = None
        self.eigenvectors = None
        self.eigenvalues = None
        self.ica_iterations = ica_iterations
        self.V_unit = None
        self.member_cutoff_mode = member_cut_off_mode
        self.assembly_members = None
        self.member_cutoff = cut_off

        self.remove_high_corr = remove_high_corr
        self.remove_high_corr_bin = remove_high_corr_bin
        self.remove_r_cutoff = remove_r_cutoff

        self.outer_product = None
        self.Rk_mat_list = []
        self.Rk_mat_df = pd.DataFrame({'epoch': [],
                                       'Rk': []})
        self.Rk_mat_jittered_list = []
        self.Rk_mat_jittered_df = pd.DataFrame({'epoch': [],
                                                'Rk': []})
        self.sample_rate_Rk = None

    def bin_spike_times(self):

        '''
        Bins spike times and stores them in self.binned_spike_train
        '''

        if self.use_spikes_in_range is None:
            self.processed_signal = bin_spike_times(self.spiking_list, self.n_samples, self.sample_rate,
                                                    self.bin_duration)
        else:
            self.binned_list_in_range = []
            for epoch in self.use_spikes_in_range:
                spikes_in_range, samples_in_epoch = spikes_in_epoch(self.spiking_list, epoch)
                self.binned_list_in_range.append(bin_spike_times(spikes_in_range, samples_in_epoch,
                                                                  self.sample_rate, self.bin_duration))
            self.processed_signal = np.concatenate(self.binned_list_in_range)

        too_few_spikes = np.sum(self.processed_signal, axis=0) < self.minimum_spikes
        self.n_neurons -= np.count_nonzero(too_few_spikes)
        if any(too_few_spikes):
            self.remove_units(too_few_spikes, 'they fire too few spikes')

        if self.remove_high_corr:
            units_too_correlate = not_too_corr([self.spiking_list[i] for i in np.where(~too_few_spikes)[0]],
                                               self.n_samples, sample_rate=self.sample_rate,
                                               bin_duration=self.remove_high_corr_bin, R_cutoff=self.remove_r_cutoff,
                                               spikes_per_neuron=None)
            self.remove_units(units_too_correlate, 'their spikes are too correlated when binned over short durations')

        self.processed_signal_prezscore = self.processed_signal.copy()

    def print_removed_units(self, remove_units_logical_array, str_reason_for_removal):
        print('The following neurons were removed because{}'.format(str_reason_for_removal),
              self.unit_id[remove_units_logical_array])

    def remove_units(self, remove_units_logical_array, str_reason_for_removal):

        self.print_removed_units(remove_units_logical_array, str_reason_for_removal)
        self.processed_signal = self.processed_signal[:, ~remove_units_logical_array]
        self.unit_id = self.unit_id[~remove_units_logical_array]

        if self.structure_list is not None:
            self.structure_list = self.structure_list[~remove_units_logical_array]

    def striding_first_dif(self):

        n_samp = int(self.bin_duration * self.sample_rate)
        self.processed_signal = self.signal[range(n_samp, self.signal.shape[0], n_samp)] - \
                                self.signal[range(0, self.signal.shape[0]-n_samp, n_samp)]

    def zscore_binned_spike_counts(self):

        '''
        Z-scores binned spike trains
        '''

        self.processed_signal = zscore(self.processed_signal)

    def PCA(self):

        '''
        Carries out PCA on self.binned_spike_train and stores eigenvalues and eigenvectors in self.eigenvalues and
        self.eigenvectors respectively.
        '''

        pca = decomposition.PCA()
        eig_store = pca.fit(self.processed_signal)
        self.eigenvalues = eig_store.explained_variance_
        self.eigenvectors = eig_store.components_

    def circshift_pca(self):

        gpu_processed_signal = torch.from_numpy(self.processed_signal).to(self.device)

        if self.gpu_circshift:
            eig_values_store = torch.zeros(self.n_neurons * self.circular_its, device=self.device)
            circshifted_rate = torch.zeros(self.processed_signal.shape, device=self.device)
            random_shift = torch.randint(0, self.processed_signal.shape[0], size=[self.n_neurons * self.circular_its])

            loop_counter = 0
            for iteration in range(self.circular_its):

                for j in range(self.n_neurons):
                    circshifted_rate[:, j] = torch.roll(gpu_processed_signal[:, j],
                                                        [int(random_shift[loop_counter].item())], dims=0)
                    loop_counter += 1

                i = range(iteration * self.n_neurons, iteration * self.n_neurons + self.n_neurons)
                eig_values_store[i], _ = pca_torch(circshifted_rate)

                print('Circular iteration number', iteration)

            self.cs_eigenvalue_dist = eig_values_store.to('cpu').numpy()

        else:
            pca = decomposition.PCA()

            self.cs_eigenvalue_dist = np.zeros(self.n_neurons * self.circular_its)
            circshifted_rate = np.zeros(self.processed_signal.shape)

            for iteration in range(self.circular_its):

                for j in range(self.n_neurons):
                    circshifted_rate[:, j] = np.roll(self.processed_signal[:, j],
                                                     random.randint(0, self.processed_signal.shape[0]), 0)

                eig_store_circshifted = pca.fit(circshifted_rate)
                i = range(iteration * self.n_neurons, iteration * self.n_neurons + self.n_neurons)
                self.cs_eigenvalue_dist[i] = eig_store_circshifted.explained_variance_

                print('Circular iteration number', iteration)

    def significance_testing(self):

        '''
        Determines significance of eigenvectors. Significant eigenvectors are stored in self.sig_eigenvectos
        '''

        if self.sig_mode == 'mp':
            self.sig_thresh = (1 + (self.processed_signal.shape[1] / self.processed_signal.shape[0])
                               ** (1 / 2)) ** 2

        if self.sig_mode == 'cs':
            self.circshift_pca()

            if self.mc_correction == 'bf':
                # Bonferroni correction
                self.sig_thresh = np.percentile(self.cs_eigenvalue_dist, 100 - self.cs_perc_cutoff / self.n_neurons)
            if self.mc_correction is None:
                self.sig_thresh = np.percentile(self.cs_eigenvalue_dist, 100 - self.cs_perc_cutoff)

        self.sig_eigenvectors = self.eigenvectors[self.eigenvalues > self.sig_thresh, :]

    def project_onto_sig_eigenvectors(self):

        '''
        Projects Binned spike data onto significant eigenvectors

        :return: Binned spike counts projected onto significant eigenvectors
        '''

        return np.matmul(self.sig_eigenvectors, self.processed_signal.T)

    def check_sig_eigenvectors(self):

        if self.sig_eigenvectors.shape[0] == 0:
            raise RuntimeError('ICA not possible because there are no significant eigenvectors')

    def ICA(self, projected_data):

        '''
        Performs ICA on binned spike counts after they have been projected onto significant eigenvectors

        :param projected_data: Binned spike counts projected onto significant eigenvectors
        :return: The unmixing matrix
        '''

        self.check_sig_eigenvectors()

        fastICA = decomposition.FastICA(max_iter=self.ica_iterations)
        unmixing_matrix = fastICA.fit(projected_data.T).components_.T

        return unmixing_matrix

    def project_unmixing_original_basis(self, unmixing_matrix):

        '''

        Projects the unmixing matrix onto the original basis

        :param unmixing_matrix: The unmixing matrix
        :return: The unmixing matrix projected on the original basis
        '''

        return np.dot(self.sig_eigenvectors.T, unmixing_matrix)

    def rescale_v(self, v):

        '''
        Rescales the projected unmixing matrix as that columns are unit length and the absolute largest weight in each
        column is positive.

        :param v: The projected unmixing matrix
        :return: The assembly pattern of ensembles.
        '''

        scale_factor_to_unit = np.sum(v ** 2, 0)
        self.V_unit = v / scale_factor_to_unit ** (1 / 2)

        max_row = np.argmax(np.abs(self.V_unit), axis=0)
        sign = np.sign(self.V_unit[max_row, range(self.V_unit.shape[1])])
        self.V_unit = sign * self.V_unit

    def check_V_unit(self):

        if self.V_unit is None:
            raise RuntimeError('Assembly patterns have not been extracted yet')

    def ensemble_members(self):

        '''

        Finds neurons with high weightings in the assembly pattern

        '''

        self.check_V_unit()

        if self.member_cutoff_mode == 'std':
            self.assembly_members = self.V_unit > (np.std(self.V_unit, axis=0) * self.member_cutoff)

        if self.member_cutoff_mode == 'perc':
            self.assembly_members = self.V_unit > np.percentile(self.V_unit, self.member_cutoff, axis=0)

    def outer_prod_V_unit(self):

        '''

        Calculates the outer product of assembly patterns. This is stored in self.outer_product where the outer
         product of ensemble_n is stored in self.outer_product[:, :, ensemble_n].

        '''

        self.check_V_unit()

        self.outer_product = outer_product_2d(self.V_unit)

    def extract_ensembles(self):

        '''

        Extracts assembly patterns

        '''
        if self.pooling_mode == 'bin':
            self.bin_spike_times()
        elif self.pooling_mode == 'dif':
            self.striding_first_dif()

        if self.zscore_spike_count:
            self.zscore_binned_spike_counts()

        self.PCA()
        self.significance_testing()
        projected_data = self.project_onto_sig_eigenvectors()

        if self.sig_eigenvectors.shape[0] == 0:
            print('No significant eigenvectors so ICA not possible')
            return

        unmixing_matrix = self.ICA(projected_data)
        V = self.project_unmixing_original_basis(unmixing_matrix)

        self.rescale_v(V)
        self.ensemble_members()
        self.outer_prod_V_unit()

    def jitter_spike_times(self, jitter_t_range):

        '''
        Jitters spike index by a random number selected from a uniform distrubution in the range -jitter_t_range to
        jitter_t_range
        :param spike_list: A list of 1d np arrays containing the spike indices of single units
        :param jitter_t_range: The range of times (s) over which jitter will be selected.
        :param n_samples: The total number of samples which spike indices are from
        :param sample_rate: The sampling rate (Hz)
        :return: The jittered spike indices of spike_list, within the range 0 to n_samples - 1
        '''

        self.jittered_spiking_list = []
        for neuron in self.spiking_list:
            jittered_neuron = jitter_in_range(neuron, jitter_t_range, self.n_samples, self.sample_rate)
            self.jittered_spiking_list.append(jittered_neuron)

    def smooth_spike_train(self, sample_rate_output=1000, n_std=5, std_bin_matched=True, std=None, epoch=None,
                           z_score=True, jitter=False, jitter_t_range=None):

        '''

        Applies Gaussian smoothing to spike trains

        :param sample_rate_output: The sample rate of the smoothed spike train
        :param n_std: The length of the Gaussian kernal in stds
        :param std_bin_matched: If true, match the std of the Gaussian to the std of a boxcar with the width of the bin
        :param std: The std of the Gaussian
        :param epoch: A 2 element array where epoch[0] is the start index and epoch[1] is the end index for the epoch
         over which the smoothed spike train is calculated.
        :param z_score: If True, the smoothed spike train is z-scored
        :return: Smoothed spike train
        '''

        assert type(z_score) == type(True), 'z_score must be a bool'
        assert type(jitter) == type(True), 'jitter must be a bool'
        assert sample_rate_output <= self.sample_rate
        assert type(std_bin_matched) == type(True)
        if not std_bin_matched:
            assert std is not None, 'std must not be None if std_bin_matched=False'

        bin_dur = 1/sample_rate_output

        if std_bin_matched:
            std = self.bin_duration * sample_rate_output / 12 ** (1 / 2)


        x = np.linspace(-np.ceil(n_std * std), np.ceil(n_std * std), int(2 * np.ceil(n_std * std) + 1))
        gaus = gaussian(x, 0, std)

        if epoch is None:
            smoothed_spike_train = np.zeros((int(self.n_samples*(sample_rate_output/self.sample_rate)),
                                             len(self.unit_id)))
        else:
            smoothed_spike_train = np.zeros((int((epoch[1]-epoch[0])*(sample_rate_output/self.sample_rate)),
                                             len(self.unit_id)))

        if jitter:
            self.jitter_spike_times(jitter_t_range)

        for i, neuron_n in enumerate(self.unit_id):

            if jitter:
                neuron = self.jittered_spiking_list[neuron_n]
            else:
                neuron = self.spiking_list[neuron_n]

            if epoch is not None:
                spikes_in_range, samples_in_epoch = spikes_in_epoch(neuron, epoch)
                binned_spikes = bin_spike_times([spikes_in_range], samples_in_epoch, sample_rate=self.sample_rate,
                                                bin_duration=bin_dur)
            else:
                binned_spikes = bin_spike_times([neuron], self.n_samples, sample_rate=self.sample_rate,
                                                bin_duration=bin_dur)
            if z_score:
                smoothed_spike_train[:, i] = zscore(scipy.signal.convolve(binned_spikes.reshape(-1), gaus, mode='same'))
            else:
                smoothed_spike_train[:, i] = scipy.signal.convolve(binned_spikes.reshape(-1), gaus, mode='same')

            print('Guassian_smoothing neuron_number:', i)

        return smoothed_spike_train

    def Rk(self, sample_rate_output=1000, n_std=5, std_bin_matched=True, std=None, epoch=None, sample_rate_epoch=20000,
           z_score=True, jitter=False, jitter_t_range=None):

        '''

        Caclulates the expression strength of an assembly pattern

        :param sample_rate_output: The sample rate of the smoothed spike train
        :param n_std: The length of the Gaussian kernal in stds
        :param std_bin_matched: If true, match the std of the Gaussian to the std of a boxcar with the width of the bin
        :param std: The std of the Gaussian
        :param epoch: A n x 2 element array where epoch[n, 0] is the start index and epoch[n, 1] is the end index of
        sub-epoch n
        :param zscore: If True, the smoothed spike train is z-scored
        :return: Expression strength of the assembly pattern
        '''

        if jitter:
            self.Rk_mat_jittered_list = []
        else:
            self.Rk_mat_list = []
        self.Rk_mat_jittered_list = []

        self.sample_rate_Rk = sample_rate_output

        epoch_Rk_df = pd.DataFrame({'epoch': [epoch],
                                   'Rk': None})

        epoch = epoch * (self.sample_rate_Rk/sample_rate_epoch)

        self.check_V_unit()

        if self.V_unit is None:
            self.extract_ensembles()
        if self.V_unit is []:
            return []

        z_smoothed = self.smooth_spike_train(sample_rate_output=sample_rate_output, n_std=n_std,
                                             std_bin_matched=std_bin_matched, std=std, z_score=z_score, jitter=jitter,
                                             jitter_t_range=jitter_t_range)

        for k, ep in enumerate(epoch.astype('int')):

            if jitter:
                self.Rk_mat_jittered_list.append(np.zeros((ep[1] - ep[0], self.V_unit.shape[1])))
            else:
                self.Rk_mat_list.append(np.zeros((ep[1] - ep[0], self.V_unit.shape[1])))

            for i in range(self.outer_product.shape[2]):
                for j, z_smooth in enumerate(z_smoothed[ep[0]:ep[1], :]):

                    if jitter:
                        self.Rk_mat_jittered_list[k][j, i] = z_smooth.reshape(1, -1) @ self.outer_product[:, :, i] \
                                                @ z_smooth.reshape(-1, 1)
                    else:
                        self.Rk_mat_list[k][j, i] = z_smooth.reshape(1, -1) @ self.outer_product[:, :, i] \
                                                @ z_smooth.reshape(-1, 1)

        if jitter:
            epoch_Rk_df['Rk'] = [self.Rk_mat_jittered_list]
            self.Rk_mat_jittered_df = pd.concat([self.Rk_mat_jittered_df, epoch_Rk_df])
        else:
            epoch_Rk_df['Rk'] = [self.Rk_mat_list]
            self.Rk_mat_df = pd.concat([self.Rk_mat_df, epoch_Rk_df])

    def member_structures(self, ensemble_number):

        '''

        Determines the anatomical structures that member neurons are in

        :param ensemble_number: The ensemble number
        :return: The structure which member neurons are part of
        '''

        return self.structure_list[self.assembly_members[:, ensemble_number]]

    def lollipop_plot(self, ensemble_number, x_label=''):
        v = self.V_unit[:, ensemble_number]
        x = np.array(range(self.V_unit.shape[0]))
        x = np.repeat(x.reshape(-1, 1), 2, axis=1)
        v_plot = np.concatenate((np.zeros(self.V_unit.shape[0]).reshape(-1, 1), v.reshape(-1, 1)), axis=1)
        plt.scatter(x[:, 0], v)
        for i, x_i in enumerate(x):
            plt.plot(x_i, v_plot[i, :], color='b')
        plt.xlabel(x_label, fontsize=15)
        plt.ylabel(ylabel='Weight', fontsize=15)
        plt.show()

    def correlation_mat(self, clim=(-0.5, 0.5)):
        if self.V_unit is None:
            self.extract_ensembles()

        if self.zscore_spike_count:
            corr_mat = (self.processed_signal.T @ self.processed_signal)/(self.processed_signal.shape[0] - 1)
        else:
            z_scored_signal = zscore(self.processed_signal)
            corr_mat = (z_scored_signal.T @ z_scored_signal)/(self.processed_signal.shape[0] - 1)

        plt.figure(figsize=[8, 8])
        plt.imshow(corr_mat, cmap='seismic')
        plt.xticks(fontsize=16)
        plt.xlabel('Channel', fontsize=25)
        plt.yticks(fontsize=16)
        plt.ylabel('Channel', fontsize=25)
        plt.clim(clim)
        cbar = plt.colorbar()
        cbar.ax.tick_params(labelsize=25)
        plt.show()
        return corr_mat
