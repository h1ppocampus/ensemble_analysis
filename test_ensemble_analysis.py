import pandas as pd

from block import BlockObj as BlO
import os
import ensemble_analysis as ea
import numpy as np
from importlib import reload
import statsmodels.api as sm
import os
import matplotlib.pyplot as plt
import scipy

sorted_files = os.listdir('Y:\Staging\Oliver\data\ks2_proc')

remove = ['gh001_210602a-gh001_210602b', 'gh010_211220a-gh010_211220b-gh010_211219a-gh010_211219b',
          'gh010_211221a-gh010_211221b-gh010_211222a-gh010_211222b']

for rem in remove:
    sorted_files.remove(rem)

ensemble_store = pd.DataFrame({'file': sorted_files})
ensemble_store['ens_full'] = None
ensemble_store['ens_dec'] = None
ensemble_store['ens_run'] = None
ensemble_store['p_values'] = None
ensemble_store['p_values_jittered'] = None
ensemble_store['p_values_full'] = None
ensemble_store['fdr_sig'] = None
ensemble_store['fdr_sig_jittered'] = None
ensemble_store['fdr_sig_full'] = None
ensemble_store['full_summary'] = None


def cross_corr_fft_kernel(X, Y, mode='same'):
    '''

    :param X: a matrix where n_col = n_variables
    :param Y: a vector containing a kernel
    :return: a matrix Z of where Z.shape = X.shape
    '''
    if X.ndim == 1:
        Z = scipy.signal.correlate(X, Y, mode=mode)
    else:
        if mode == 'full':
            Z = np.zeros((X.shape[0]+Y.shape[0]-1, X.shape[1]))
            for i in range(X.shape[1]):
                Z[:, i] = scipy.signal.correlate(X[:, i], Y, mode=mode)
        elif mode == 'same':
            Z = np.zeros(X.shape)
            for i in range(X.shape[1]):
                Z[:, i] = scipy.signal.correlate(X[:, i], Y, mode=mode)
        else:
            raise ValueError('mode not supported')

    return Z


def mean_kernel(index_duration):
    return np.ones(index_duration)/index_duration


def spikes_in_range(spike_index, t_ind_start, t_ind_end):
    return spike_index[np.logical_and(spike_index>t_ind_start, spike_index<t_ind_end)] - t_ind_start


def benjamini_hochberg(p_values, fdr=0.05):
    p_sorted = np.sort(p_values)
    m = p_sorted.shape[0]
    max_index = np.nan
    for i in range(p_sorted.shape[0]):
        if p_sorted[i] < fdr*(i+1)/m:
            max_index = i

    if ~np.isnan(max_index):
        sig_p_values = p_values <= p_sorted[max_index]
    else:
        sig_p_values = np.zeros(p_values.shape[0]).astype('bool_')

    return sig_p_values

struct = ['mo-right', 'str-right', 'stn-right', 'vp-right', 'acc-right']

corridor_lightgates = {'beh_ch_lab': ['C_LG_1_A', 'C_LG_2_A', 'C_LG_3_A', 'C_LG_1_B', 'C_LG_2_B', 'C_LG_3_B']}


dir = os.getcwd()  # Configure a directory where the output files will be saved (one per each data stream)

for file_i, file in enumerate(ensemble_store['file']):

    try:
        specs = {'block': [file[0:13], file[14:29]],
                 'stage': 'all',
                 'environment': ['oliver_open_field', 'radmac'],
                 'block_comment': 'all'
                 }

        beh_specs = {'block': [file[14:29]],
                     'stage': 'all',
                     'environment': ['oliver_open_field', 'radmac'],
                     'block_comment': 'all'
                     }

        # Initialise the meta using the info about the block, stage(s) and environemnt(s)
        meta = BlO(**specs)
        beh_meta = BlO(**beh_specs)
        # , 'str-right', 'stn-right', 'acc-right', 'vp-right'

        din_specs = {'region': 'D-IN', 'ch_name': 'beh-ttl'}
        din = BlO.DinChannels(meta=beh_meta, **din_specs).get_objects()

        beh_ch_specs = {'beh_ch_lab': ['run_trial']}
        run = BlO.FineBehaviour(meta=meta, **beh_ch_specs).get_objects()
        beh_ch_specs = {'beh_ch_lab': ['omit_trial']}
        omit = BlO.FineBehaviour(meta=meta, **beh_ch_specs).get_objects()
        beh_ch_specs = {'beh_ch_lab': ['DEL']}
        delay = BlO.FineBehaviour(meta=meta, **beh_ch_specs).get_objects()
        beh_ch_specs = {'beh_ch_lab': ['CUE']}
        cue = BlO.FineBehaviour(meta=meta, **beh_ch_specs).get_objects()

        block = specs['block'][1]
        stages = specs['stage']
        block_com = specs['block_comment']
        tb = BlO.TrialBehaviour(BlO(block=block, stage=stages, environment='radmac', block_comment=block_com)).get_trial_data()

        tb.beh_df['trial_start'] = np.nan
        tb.beh_df['trial_end'] = np.nan
        tb.beh_df['decision_start'] = np.nan
        tb.beh_df['pre-decision'] = np.nan
        tb.beh_df['decision_end'] = np.nan
        tb.beh_df['post-decision'] = np.nan
        tb.beh_df['decision_full'] = np.nan
        tb.beh_df['run_start'] = np.nan
        tb.beh_df['run_end'] = np.nan
        tb.beh_df['Rk_total'] = np.nan

        total_time_stage_end = np.zeros(len(din.ol))
        for i, din_ol_i in enumerate(din.ol):
            if i == len(din.ol) - 1:
                break
            else:
                total_time_stage_end[i+1] = total_time_stage_end[i] + din_ol_i.fall[-1] - din_ol_i.rise[0]

        for i, stage in enumerate(tb.beh_df['stage'].unique()):

            correct_stage = tb.beh_df['stage'] == stage
            run_correct_stage = np.logical_and(correct_stage, tb.beh_df['far_mag_entry'] == 1)
            nill = tb.beh_df['reward'] == 0
            rewarded = np.logical_not(nill)
            nill_short = tb.beh_df[run_correct_stage]['reward'] == 0
            rewarded_short = np.logical_not(nill_short)
            omit_correct_stage = np.logical_and(correct_stage, tb.beh_df['far_mag_entry'] == 0)

            begin_trial = np.zeros(len(run.ol[i].rise()))
            temp_array = np.zeros(len(run.ol[i].rise()))
            for j, start_trial in enumerate(run.ol[i].rise()):
                begin_trial[j] = cue.ol[i].rise()[np.where(cue.ol[i].rise() > start_trial)[0][0]] * 20
                temp_array[j] = begin_trial[j] + total_time_stage_end[i]
            tb.beh_df['trial_start'][run_correct_stage] = temp_array

            lg_specs = {'block': specs['block'][1],
                        'stage': stage,
                        'environment': ['radmac'],
                        'block_comment': 'all'
                        }
            lg_meta = BlO(**lg_specs)
            corridor_entry = BlO.FineBehaviour(meta=lg_meta, **corridor_lightgates).get_objects()
            first_corridor_entry = np.zeros(run.ol[i].rise().shape[0])
            light_gate_not_trigger = 0
            for n_trial, trial_start in enumerate(begin_trial):
                for n_lg in range(len(corridor_entry.ol)):
                    i_corr_ent = np.where(corridor_entry.ol[n_lg].rise()*20 > trial_start)[0]
                    if i_corr_ent.shape[0]>0:
                        cor_ent =corridor_entry.ol[n_lg].rise()[i_corr_ent[0]]*20
                        if n_lg == 0:
                            first_corridor_entry[n_trial] = cor_ent
                        elif cor_ent < first_corridor_entry[n_trial]:
                            first_corridor_entry[n_trial] = cor_ent
                    else:
                        light_gate_not_trigger += 1

            run_start = first_corridor_entry
            run_end = delay.ol[i].fall()*20 + 3 * 20000  # 3s post reward delivery
            outside_stage = run_end > din.ol[i].fall[-1]
            run_end[outside_stage] = din.ol[i].fall[-1]

            temp_array = np.zeros(len(omit.ol[i].rise()))
            for j, start_trial in enumerate(omit.ol[i].rise()):
                temp_array[j] = cue.ol[i].rise()[np.where(cue.ol[i].rise() > start_trial)[0][0]]*20 + total_time_stage_end[i]
            tb.beh_df['trial_start'][omit_correct_stage] = temp_array

            tb.beh_df['trial_end'][np.logical_and(run_correct_stage, rewarded)] = run_end + total_time_stage_end[i]
            tb.beh_df['trial_end'][np.logical_and(run_correct_stage, nill)] = run.ol[i].fall()[nill_short]*20 \
                                                                              + total_time_stage_end[i]
            # find first eval fall after omit
            tb.beh_df['trial_end'][omit_correct_stage] = omit.ol[i].fall()*20 + total_time_stage_end[i]
            tb.beh_df['decision_end'][run_correct_stage] = run_start - 1 + total_time_stage_end[i]
            tb.beh_df['decision_end'][omit_correct_stage] = tb.beh_df['trial_start'][omit_correct_stage] + \
                                                            np.mean(tb.beh_df['decision_end'][run_correct_stage]
                                                                    - tb.beh_df['trial_start'][run_correct_stage])
            tb.beh_df['decision_full'][omit_correct_stage] = tb.beh_df['trial_end'][omit_correct_stage]
            tb.beh_df['decision_full'][run_correct_stage] = tb.beh_df['decision_end'][run_correct_stage]
            tb.beh_df['run_start'][run_correct_stage] = run_start + total_time_stage_end[i]
            tb.beh_df['run_end'][np.logical_and(run_correct_stage, rewarded)] = run_end + total_time_stage_end[i]
            tb.beh_df['run_end'][np.logical_and(run_correct_stage, nill)] = run.ol[i].fall()[nill_short]*20 \
                                                                            + total_time_stage_end[i]

        # add ommit run_start and end
        tb.beh_df['decision_start'] = tb.beh_df['trial_start']
        tb.beh_df['pre-decision'] = tb.beh_df['decision_start'] - 20000
        tb.beh_df['post-decision'] = tb.beh_df['decision_end'] + 20000 * 2


        spike_list = []
        structure_list = []
        k = 0
        for struct_i in struct:
            sp = BlO.SpikeTrains(meta=meta, structure=[struct_i]).get_objects()
            for i in range(beh_meta.env_meta['stage'].size):
                for j, neuron in enumerate(sp.ol[i+1].spike_times):
                    if i == 0:
                        spike_list.append(spikes_in_range(neuron, din.ol[i].rise[0], din.ol[i].fall[-1]))
                        structure_list.append(struct_i)
                    else:
                        spike_list[j+k] = np.concatenate([spike_list[j+k], spikes_in_range(neuron, din.ol[i].rise[0],
                                                                                           din.ol[i].fall[-1])
                                                        + total_time_stage_end[i]])
            k += len(sp.ol[i].spike_times)

        epoch = np.concatenate((tb.beh_df['trial_start'].to_numpy().reshape(-1, 1),
                                tb.beh_df['trial_end'].to_numpy().reshape(-1, 1)), axis=1)
        epoch2 = np.concatenate((tb.beh_df['decision_start'].to_numpy().reshape(-1, 1),
                                tb.beh_df['decision_end'].to_numpy().reshape(-1, 1)), axis=1)
        epoch3 = np.concatenate((tb.beh_df['run_start'].to_numpy().reshape(-1, 1),
                                tb.beh_df['run_end'].to_numpy().reshape(-1, 1)), axis=1)
        epoch3 = epoch3[np.isnan(epoch3[:, 0]) == 0]
        epoch4 = np.concatenate((tb.beh_df['pre-decision'].to_numpy().reshape(-1, 1),
                                tb.beh_df['post-decision'].to_numpy().reshape(-1, 1)), axis=1)

        ens1 = ea.AssemblyPattern(spike_list, total_time_stage_end[-1] + din.ol[-1].fall[-1],
                                  use_spikes_in_range=epoch, structure_list=structure_list)
        ens1.extract_ensembles()
        ens2 = ea.AssemblyPattern(spike_list, total_time_stage_end[-1] + din.ol[-1].fall[-1],
                                  use_spikes_in_range=epoch2, structure_list=structure_list)
        ens2.extract_ensembles()
        ens3 = ea.AssemblyPattern(spike_list, total_time_stage_end[-1] + din.ol[-1].fall[-1],
                                  use_spikes_in_range=epoch3, structure_list=structure_list)
        ens3.extract_ensembles()

        # R1 = np.matmul(ens1.V_unit.T, ens2.V_unit)
        # R2 = np.matmul(ens1.V_unit.T, ens3.V_unit)

        ens1.Rk(epoch=epoch4)
        ens1.Rk(epoch=epoch4, jitter=True, jitter_t_range=0.1)

        ens1.Rk(epoch=epoch2)
        print('made it to 1')
        ens1.Rk(epoch=epoch2, jitter=True, jitter_t_range=0.1)
        print('made it to 2')

        Rk_store = np.zeros((len(ens1.Rk_mat_list), ens1.V_unit.shape[1]))
        Rk_store_jittered = np.zeros((len(ens1.Rk_mat_jittered_list), ens1.V_unit.shape[1]))
        for i, Rk_mat in enumerate(ens1.Rk_mat_list):
            Rk_store[i, :] = np.mean(Rk_mat, axis=0)
        for i, Rk_mat_jittered in enumerate(ens1.Rk_mat_jittered_list):
            Rk_store_jittered[i, :] = np.mean(Rk_mat_jittered, axis=0)

        print('3')

        cols = ['effort_lev', 'trial_order', 'far_mag_entry']
        x = tb.beh_df[cols]

        p_values = np.zeros((3, Rk_store.shape[1]))
        for i, Rk_i in enumerate(Rk_store.T):
            x = sm.add_constant(x)
            mod = sm.OLS(Rk_i, x)
            results = mod.fit()
            p_values[:, i] = results.pvalues[1:]

        x = tb.beh_df[cols]
        p_values_jittered = np.zeros((3, Rk_store_jittered.shape[1]))
        for i, Rk_i in enumerate(Rk_store_jittered.T):
            x = sm.add_constant(x)
            mod = sm.OLS(Rk_i, x)
            results = mod.fit()
            p_values_jittered[:, i] = results.pvalues[1:]

        x = tb.beh_df[cols]
        p_values_full = np.zeros((4, Rk_store_jittered.shape[1]))
        results_list = []
        for i, Rk_i in enumerate(Rk_store_jittered.T):
            x = sm.add_constant(x)
            x['Rk_jittered'] = Rk_i
            mod = sm.OLS(Rk_store[:, i], x)
            results = mod.fit()
            results_list.append(results)
            p_values_full[:, i] = results.pvalues[1:]

        print('4')

        sig_p_vals = benjamini_hochberg(p_values.reshape(-1))
        sig_p_vals_2d = sig_p_vals.reshape(3, -1)

        sig_p_vals_jittered = benjamini_hochberg(p_values_jittered.reshape(-1))
        sig_p_vals_2d_jittered = sig_p_vals_jittered.reshape(3, -1)

        sig_p_vals_full = benjamini_hochberg(p_values_full.reshape(-1))
        sig_p_vals_2d_full = sig_p_vals_full.reshape(4, -1)

        ensemble_store['ens_full'].iloc[file_i] = ens1
        ensemble_store['ens_dec'].iloc[file_i] = ens2
        ensemble_store['ens_run'].iloc[file_i] = ens3
        ensemble_store['p_values'].iloc[file_i] = p_values
        ensemble_store['p_values_jittered'].iloc[file_i] = p_values_jittered
        ensemble_store['p_values_full'].iloc[file_i] = p_values_full
        ensemble_store['fdr_sig'].iloc[file_i] = sig_p_vals_2d
        ensemble_store['fdr_sig_jittered'].iloc[file_i] = sig_p_vals_2d_jittered
        ensemble_store['fdr_sig_full'].iloc[file_i] = sig_p_vals_2d_full
        ensemble_store['full_summary'].iloc[file_i] = results_list

        print('Successfully completed {}'.format(file))

    except:
        print('Error in {}'.format(file))


effort_members = np.zeros(5)
effort_all = np.zeros(5)
reward_members = np.zeros(5)
reward_all = np.zeros(5)
accept_members = np.zeros(5)
accept_all = np.zeros(5)
block_count = np.zeros(5)
none_members = np.zeros(5)
none_all = np.zeros(5)
for i, sig in enumerate(ensemble_store['fdr_sig']):
    if sig is not None:
        for j, struct_i in enumerate(struct):

            block_count[j] = np.sum(ensemble_store['ens_full'].iloc[i].structure_list == struct_i)
        for where in np.where(sig[0, :])[0]:
            if where.size > 0:
                for j, struct_i in enumerate(struct):
                    effort_members[j] += np.sum(ensemble_store['ens_full'].iloc[i].member_structures(where) == struct_i)
                    effort_all[j] += block_count[j]
        for where in np.where(sig[1, :])[0]:
            if where.size > 0:
                for j, struct_i in enumerate(struct):
                    reward_members[j] += np.sum(ensemble_store['ens_full'].iloc[i].member_structures(where) == struct_i)
                    reward_all[j] += block_count[j]
        for where in np.where(sig[2, :])[0]:
            if where.size > 0:
                for j, struct_i in enumerate(struct):
                    accept_members[j] += np.sum(ensemble_store['ens_full'].iloc[i].member_structures(where) == struct_i)
                    accept_all[j] += block_count[j]
        for where in np.where((sig[0, :] | sig[1, :] | sig[2, :]) == 0)[0]:
            if where.size > 0:
                for j, struct_i in enumerate(struct):
                    none_members[j] += np.sum(ensemble_store['ens_full'].iloc[i].member_structures(where) == struct_i)
                    none_all[j] += block_count[j]

labels = ['MO', 'Str', 'STN', 'VP', 'Acc']
effort = effort_members/effort_all
reward = reward_members/reward_all
accept = accept_members/accept_all
none = none_members/none_all

x = np.arange(len(labels))  # the label locations
width = 0.15  # the width of the bars

fig, ax = plt.subplots()
rects1 = ax.bar(x - width*1.5, effort, width, label='Effort')
rects2 = ax.bar(x - width/2, reward, width, label='Reward')
rects3 = ax.bar(x + width/2, accept, width, label='Accept/Reject')
rects4 = ax.bar(x + 1.5*width, none, width, label='None')

# Add some text for labels, title and custom x-axis tick labels, etc.
ax.set_ylabel('Proportion of neurons', fontsize=25)
ax.set_xlabel('Structure', fontsize=25)
ax.set_title('Proportion of neurons which are ensemble members', fontsize=30)
ax.set_xticks(x)
ax.set_xticklabels(labels, fontsize=20)
plt.yticks(fontsize=20)
ax.legend(fontsize=20)

fig.tight_layout()

plt.show()

count_sig_ensembles = np.zeros(3)
count_sig_reward_effort = 0
count_sig_reward_accept = 0
count_sig_effort_accept = 0
count_sig_all = 0
count_sig_ensembles_jittered = np.zeros(3)
total_ensembles = 0

for sig in ensemble_store['fdr_sig_full']:
    if sig is not None:
        count_sig_ensembles += np.count_nonzero(sig[0:3, :], axis=1)
        count_sig_reward_effort += np.count_nonzero(sig[0, :] & sig[1, :])
        count_sig_reward_accept += np.count_nonzero(sig[0, :] & sig[2, :])
        count_sig_effort_accept += np.count_nonzero(sig[1, :] & sig[2, :])
        count_sig_all += np.count_nonzero(sig[0,:] & sig[1, :] & sig[2, :])
        total_ensembles += sig.shape[1]


for sig in ensemble_store['fdr_sig_jittered']:
    if sig is not None:
        count_sig_ensembles_jittered += np.count_nonzero(sig, axis=1)

member_store = pd.DataFrame({'reward': np.zeros(total_ensembles)})

member_store['effort'] = np.nan
member_store['accept'] = np.nan
member_store['reward'] = np.nan
member_store['member'] = np.nan
member_store['member_unique'] = np.nan
member_store['member_n_structs'] = np.nan
member_store['struct_list'] = np.nan

a = 0
for j, ens in enumerate(ensemble_store['ens_full']):
    p_val = ensemble_store['fdr_sig_full'].iloc[j]
    if ens is not None:
        for i in range(ens.V_unit.shape[1]):
            member_store['effort'].iloc[a] = p_val[0, i]
            member_store['accept'].iloc[a] = p_val[1, i]
            member_store['reward'].iloc[a] = p_val[2, i]
            member_store['member'].iloc[a] = ens.member_structures(i)
            member_store['member_unique'].iloc[a] = np.unique(ens.member_structures(i))
            member_store['member_n_structs'].iloc[a] = member_store['member_unique'].iloc[a].size
            member_store['struct_list'].iloc[a] = ens.structure_list
            a += 1

cols = ['effort_lev', 'trial_order', 'far_mag_entry']
time_duration = 3000
time_series_store = []
for k, ens_file in enumerate(ensemble_store['file']):
    specs = {'block': [ens_file[0:13], ens_file[14:29]],
             'stage': 'all',
             'environment': ['oliver_open_field', 'radmac'],
             'block_comment': 'all'
             }
    block = specs['block'][1]
    stages = specs['stage']
    block_com = specs['block_comment']
    tb = BlO.TrialBehaviour(
        BlO(block=block, stage=stages, environment='radmac', block_comment=block_com)).get_trial_data().beh_df

    ens = ensemble_store['ens_full'].iloc[k]
    if ens is not None:
        for j in range(ens.Rk_mat_list[0].shape[1]):
            Rk_smoothed_start = np.zeros((time_duration, len(tb)))
            Rk_smoothed_jittered_start = np.zeros((time_duration, len(tb)))
            Rk_smoothed_end = np.zeros((time_duration, len(tb)))
            Rk_smoothed_jittered_end = np.zeros((time_duration, len(tb)))

            for l in range(len(ens.Rk_mat_list)):
                Rk_smoothed_start[:, l] = cross_corr_fft_kernel(ens.Rk_mat_df['Rk'].iloc[0][l][0:time_duration, j],
                                                                mean_kernel(200))
                Rk_smoothed_jittered_start[:, l] = cross_corr_fft_kernel(ens.Rk_mat_jittered_df['Rk'].iloc[0][l]
                                                                         [0:time_duration, j], mean_kernel(200))
                # Rk_smoothed_end[:, l] = cross_corr_fft_kernel(ens.Rk_mat_df['Rk'].iloc[0][l][-time_duration:, j],
                #                                               mean_kernel(200))
                # Rk_smoothed_jittered_end[:, l] = cross_corr_fft_kernel(ens.Rk_mat_jittered_df['Rk'].iloc[0][l]
                #                                                        [-time_duration:, j], mean_kernel(200))

            time_series_reg = np.zeros((Rk_smoothed_start.shape[0], 4))
            x = tb[cols]
            x = sm.add_constant(x)
            for t, Rk_t_jittered_start in enumerate(Rk_smoothed_jittered_start):
                x['Rk_jittered'] = Rk_t_jittered_start
                mod = sm.OLS(Rk_smoothed_start[t, :], x)
                results = mod.fit()
                time_series_reg[t, :] = results.params.values[1:5]

            time_series_store.append(time_series_reg.reshape(time_series_reg.shape[0], time_series_reg.shape[1], 1))

time_series_mat = np.concatenate(time_series_store, axis=2)

sig_concat = np.zeros((0, 4))
for sig in ensemble_store['fdr_sig_full']:
    if sig is not None:
        sig_concat = np.concatenate((sig_concat, sig.T), axis=0)

positive_corr = np.zeros((0,4))
for list_result in ensemble_store['full_summary']:
    if list_result is not None:
        for result_i in list_result:
            positive_corr = np.concatenate((positive_corr, result_i.params.values[1:5].reshape(1, 4) > 0), axis=0)


for type in range(3):
    for positive_log in range(2):
        mean_store = np.mean(time_series_mat[:, type, np.logical_and(positive_corr[:, type]==positive_log,
                                                             sig_concat[:, type] == 1)], axis=1)
        std_err = np.std(time_series_mat[:, type, np.logical_and(positive_corr[:, type]==positive_log,
                                                             sig_concat[:, type] == 1)], axis=1)/\
                  np.count_nonzero(np.logical_and(positive_corr[:, type]==positive_log, sig_concat[:, type] == 1)) ** \
                  (1/2)
        mean_non = np.mean(time_series_mat[:, type, np.logical_and(positive_corr[:, type]==positive_log,
                                                             sig_concat[:, type] == 0)], axis=1)
        non_std_err = np.std(time_series_mat[:, type, np.logical_and(positive_corr[:, type]==positive_log,
                                                             sig_concat[:, type] == 1)], axis=1)/\
                  np.count_nonzero(np.logical_and(positive_corr[:, type]==positive_log, sig_concat[:, type] == 1)) ** \
                  (1/2)
        plt.plot(range(-1000, 2000), mean_store, color='b')
        plt.fill_between(x=range(-1000, 2000), y1=mean_store + std_err, y2=mean_store - std_err, color='b', alpha=0.5)
        plt.plot(range(-1000, 2000), mean_non, color='r')
        plt.fill_between(x=range(-1000, 2000), y1=mean_non + non_std_err, y2=mean_non - non_std_err, color='r',
                         alpha=0.5)
        plt.xlabel('Time (ms)', fontsize=30)
        plt.ylabel('Regression coef', fontsize=30)
        plt.xticks(fontsize=25)
        plt.yticks(fontsize=25)
        plt.show()


plt.plot(ensemble_store['ens_full'].iloc[2].Rk_mat_list[1][:, 0], label='Non-jittered')
plt.plot(ensemble_store['ens_full'].iloc[2].Rk_mat_jittered_list[1][:, 0], label='Jittered')
plt.xlabel('Time (ms)', fontsize=30)
plt.ylabel('Coactivity', fontsize=30)
plt.xticks(fontsize=25)
plt.yticks(fontsize=25)
plt.legend(fontsize=25)
plt.show()

prop_pos = np.count_nonzero(np.logical_and(positive_corr, sig_concat), axis=0)/np.count_nonzero(sig_concat, axis=0)